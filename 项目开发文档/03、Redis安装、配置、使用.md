#### 一、终端命令安装

```shell
# step1:下载
	wget http://download.redis.io/releases/redis-3.2.8.tar.gz

# step2:解压
    tar -zxvf redis-3.2.8.tar.gz

# step3:复制，放到usr/local⽬录下 ("."表示当前目录)
    sudo mv ./redis-3.2.8  /usr/local/redis/

# step4:进⼊redis⽬录
    cd /usr/local/redis/

# step5:生成可执行文件
    sudo make

# step6:测试,这段运⾏时间会较⻓
    sudo make test

# step7:安装,此时会将redis的命令安装到/usr/local/bin/⽬录
    sudo make install

# step8:安装完成后，我们进入目录/usr/local/bin中查看
    cd /usr/local/bin
    ls -all

# step9:配置⽂件，移动到/etc/⽬录下
	配置⽂件⽬录为/usr/local/redis/redis.conf
	sudo cp /usr/local/redis/redis.conf /etc/redis/
```

#### 二、配置:

```shell
Redis的配置信息在/etc/redis/redis.conf下。
查看  sudo vi /etc/redis/redis.conf

核心配置选项
   # 绑定ip：如果需要远程访问，可将此⾏注释，或绑定⼀个真实ip
        bind 127.0.0.1
   # 端⼝，默认为6379
       	port 6379
   # 是否以守护进程运⾏
        如果以守护进程运⾏，则不会在命令⾏阻塞，类似于服务
        如果以⾮守护进程运⾏，则当前终端被阻塞
        设置为yes表示守护进程，设置为no表示⾮守护进程
        推荐设置为yes
        daemonize yes
   # 数据⽂件
        dbfilename dump.rdb
   # 数据⽂件存储路径
        dir /var/lib/redis
   # ⽇志⽂件
        logfile /var/log/redis/redis-server.log
   # 数据库，默认有16个
        database 16
   # 主从复制，类似于双机备份。
        slaveof 
```

#### 三、启动

```shell
# 查看redis服务器进程
ps -ef|grep redis 
# 杀死redis服务器
sudo kill -9 pid 
# 指定加载的配置文件 启动服务端
sudo redis-server /etc/redis/redis.conf 
# 启动客户端
redis-cli
redis-cli -h 192.168.182.132 -p 8080
# 运⾏测试命令 
ping
# 切换数据库
select n
```

#### 四、数据类型及命令

[redis命令参考文档](http://doc.redisfans.com/)

##### 1、string类型:

```
字符串类型是Redis中最为基础的数据存储类型，它在Redis中是二进制安全的，这便意味着该类型可以接受任何格式的数据，如JPEG图像数据或Json对象描述信息等。在Redis中字符串类型的Value最多可以容纳的数据长度是512M。

命令:保存和获取
set 
get
mset
mget
setex
```

```
查找键,参数⽀持正则表达式
keys *
keys 'a*'         查看名称中包含a的键
exists key1 	判断键是否存在，如果存在返回1，不存在返回0
type key 		 查看键对应的value的类型
del key1 key2 ... 删除键及对应的值
设置过期时间，以秒为单位
如果没有指定过期时间则⼀直存在，直到使⽤DEL移除
expire key seconds
查看有效时间，以秒为单位
ttl key
```

##### 2、hash类型:

```
hash⽤于存储对象，对象的结构为属性、值 ，值的类型为string
1）增加、修改
设置单个属性
 	 hset key field value
例1：设置键 user的属性name为xm
    hset user name xm
设置多个属性
    hmset key field1 value1 field2 value2 ...
例2：设置键u2的属性name为dm、属性age为11
    hmset u2 name dm age 11
```

```
2）可能出现报错：
MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled. Please check Redis logs for details about the error.
Redis被配置为保存数据库快照，但它目前不能持久化到硬盘。用来修改集合数据的命令不能用
原因： 强制关闭Redis快照导致不能持久化。 
解决方案：
运行config set stop-writes-on-bgsave-error no　命令后，关闭配置项stop-writes-on-bgsave-error解决该问题。
```

```
3）获取
获取指定键所有的属性
    hkeys key
获取⼀个属性的值
    hget key field
例4：获取键u2属性'name'的值
    hget u2 'name'
获取多个属性的值
    hmget key field1 field2 ...
例5：获取键u2属性'name'、'age的值
    hmget u2 name age
获取所有属性的值
    hvals key
例6：获取键'u2'所有属性的值
    hvals u2
```

```
4）删除
删除整个hash键及值，使⽤del命令
删除属性，属性对应的值会被⼀起删除
    hdel key field1 field2 ...
```

##### 3、list类型

```shell
列表的元素类型为string，按照插⼊顺序排序

 1）增加

- 在左侧插⼊数据

  > lpush key value1 value2 ...

- 例1：从键为'a1'的列表左侧加⼊数据a 、 b 、c

  > lpush a1 a b c

- 在右侧插⼊数据

  > rpush key value1 value2 ...

- 例2：从键为'a1'的列表右侧加⼊数据0 1

  > rpush a1 0 1

- 在指定元素的前或后插⼊新元素

  > linsert key before或after 现有元素 新元素

- 例3：在键为'a1'的列表中元素'b'前加⼊'3'

  > linsert a1 before b 3
------------------------------------------------------
2）获取

- 返回列表⾥指定范围内的元素
  - start、stop为元素的下标索引
  - 索引从左侧开始，第⼀个元素为0
  - 索引可以是负数，表示从尾部开始计数，如-1表示最后⼀个元素

  > lrange key start stop

- 例4：获取键为'a1'的列表所有元素

  > lrange a1 0 -1
------------------------------------------------------
3）设置指定索引位置的元素值

- 索引从左侧开始，第⼀个元素为0

- 索引可以是负数，表示尾部开始计数，如-1表示最后⼀个元素

  > lset key index value

- 例5：修改键为'a1'的列表中下标为1的元素值为'z'

  > lset a 1 z
------------------------------------------------------
 4）删除

- 删除指定元素

  - 将列表中前count次出现的值为value的元素移除
  - count > 0: 从头往尾移除
  - count < 0: 从尾往头移除
  - count = 0: 移除所有

  > lrem key count value

- 例6.1：向列表'a2'中加⼊元素'a'、'b'、'a'、'b'、'a'、'b'

  > lpush a2 a b a b a b

- 例6.2：从'a2'列表右侧开始删除2个'b'

  > lrem a2 -2 b

- 例6.3：查看列表'py12'的所有元素

  > lrange a2 0 -1
```

##### 4、set类型

```shell
- ⽆序集合
- 元素为string类型
- 元素具有唯⼀性，不重复
- 说明：对于集合没有修改操作

1）增加

- 添加元素

  > sadd key member1 member2 ...

- 例1：向键'a3'的集合中添加元素'zhangsan'、'lisi'、'wangwu'

  > sadd a3 zhangsan sili wangwu
------------------------------------------------------
2）获取

- 返回所有的元素

  > smembers key

- 例2：获取键'a3'的集合中所有元素

  > smembers a3
------------------------------------------------------
3）删除

- 删除指定元素

  > srem key

- 例3：删除键'a3'的集合中元素'wangwu'

  > srem a3 wangwu
```



##### 5、zset类型

```shell
- sorted set，有序集合
- 元素为string类型
- 元素具有唯⼀性，不重复
- 每个元素都会关联⼀个double类型的score，表示权重，通过权重将元素从⼩到⼤排序
- 说明：没有修改操作

 1）增加
- 添加

  > zadd key score1 member1 score2 member2 ...

- 例1：向键'a4'的集合中添加元素'lisi'、'wangwu'、'zhaoliu'、'zhangsan'，权重分别为4、5、6、3

  > zadd a4 4 lisi 5 wangwu 6 zhaoliu 3 zhangsan
------------------------------------------------------
 2）获取

- 返回指定范围内的元素

- start、stop为元素的下标索引

- 索引从左侧开始，第⼀个元素为0

- 索引可以是负数，表示从尾部开始计数，如-1表示最后⼀个元素

  > zrange key start stop

- 例2：获取键'a4'的集合中所有元素

  > zrange a4 0 -1


- 返回score值在min和max之间的成员

  > zrangebyscore key min max

- 例3：获取键'a4'的集合中权限值在5和6之间的成员

  > zrangebyscore a4 5 6

- 返回成员member的score值

  > zscore key member

- 例4：获取键'a4'的集合中元素'zhangsan'的权重

  > zscore a4 zhangsan
------------------------------------------------------
3）删除

- 删除指定元素

  > zrem key member1 member2 ...

- 例5：删除集合'a4'中元素'zhangsan'

  > zrem a4 zhangsan


- 删除权重在指定范围的元素

  > zremrangebyscore key min max

- 例6：删除集合'a4'中权限在5、6之间的元素

  > zremrangebyscore a4 5 6

```

redis主从

集群:一组通过网络连接的计算机，共同对外提供服务，像一个独立的服务器。



#### 五、应用

##### 1、redis保存用户的sessionID 

```python
Django的缓存配置

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://192.168.182.132:6379/9",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# 配置session的存储  他有三种存储
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# 例如在redis的键值对：
":1:django.contrib.sessions.cachergjvhoi8stdk8s6jhls22dq5pxtdgc98"  key    value


```

##### 2、redis保存、获取用户的历史浏览记录

以list类型存储

##### 3、redis保存用户购物车记录

![image-20201207114213448](C:\Users\16096\AppData\Roaming\Typora\typora-user-images\image-20201207114213448.png)

```python
 from django_redis import get_redis_connection
 # 5.获取用户购物车中商品的数目
    user = request.user
    cart_count = 0
    if user.is_authenticated():
        # 用户已登录
        conn = get_redis_connection('default')
        cart_key = 'cart_%d' % user.id
        cart_count = conn.hlen(cart_key)

    # 组织模板上下文
    context.update(cart_count=cart_count)
```


```html
	<div class="guest_cart fr">
		<a href="#" class="cart_name fl">我的购物车</a>
		<div class="goods_count fl" id="show_count">{{ cart_count }}</div>
	</div>
```

##### 4、设置首页静态缓存 cache 到 redis中

```python
context = {'types': types,
'goods_banners': goods_banners,
'promotion_banners': promotion_banners}

# 设置缓存 有时，缓存整个页面不会让你获益很多，事实上，过度的矫正原来的不方便，变得更加不方便。
# Django提供了一个底层的 cache API. 你可以用这个 API来储存在缓存中的对象，并且控制力度随意
# 您可以缓存可以安全pickle的任何Python对象：模型对象的字符串，字典，列表等等。
# 最基本的接口是 set(key, value, timeout) 和 get(key):

# 设置首页的缓存数据key  value timeout(s)
cache.set('index_page_data', context, 3600)

# 从缓存中获取数据 --> 得不到返回None
context = cache.get('index_page_data')

 # 清除首页的缓存数据
cache.delete('index_page_data')
```

#### 六、常用命令

```
1、连接操作相关的命令
    quit：关闭连接（connection）
    auth：简单密码认证

2、对value操作的命令
    exists(key)：确认一个key是否存在
    del(key)：删除一个key
    type(key)：返回值的类型
    keys(pattern)：返回满足给定pattern的所有key
    randomkey：随机返回key空间的一个key
    rename(oldname, newname)：将key由oldname重命名为newname，若newname存在则删除newname表示的key
    dbsize：返回当前数据库中key的数目
    expire：设定一个key的活动时间（s）
    ttl：获得一个key的活动时间
    select(index)：按索引查询
    move(key, dbindex)：将当前数据库中的key转移到有dbindex索引的数据库
    flushdb：删除当前选择数据库中的所有key
    flushall：删除所有数据库中的所有key

3、对String操作的命令
    set(key, value)：给数据库中名称为key的string赋予值value
    get(key)：返回数据库中名称为key的string的value
    getset(key, value)：给名称为key的string赋予上一次的value
    mget(key1, key2,…, key N)：返回库中多个string（它们的名称为key1，key2…）的value
    setnx(key, value)：如果不存在名称为key的string，则向库中添加string，名称为key，值为value
    setex(key, time, value)：向库中添加string（名称为key，值为value）同时，设定过期时间time
    mset(key1, value1, key2, value2,…key N, value N)：同时给多个string赋值，名称为key i的string赋值value i
    msetnx(key1, value1, key2, value2,…key N, value N)：如果所有名称为key i的string都不存在，则向库中添加string，名称key i赋值为value i
    incr(key)：名称为key的string增1操作
    incrby(key, integer)：名称为key的string增加integer
    decr(key)：名称为key的string减1操作
    decrby(key, integer)：名称为key的string减少integer
    append(key, value)：名称为key的string的值附加value
    substr(key, start, end)：返回名称为key的string的value的子串

4、对List操作的命令
    rpush(key, value)：在名称为key的list尾添加一个值为value的元素
    lpush(key, value)：在名称为key的list头添加一个值为value的 元素
    llen(key)：返回名称为key的list的长度  
    lrange(key, start, end)：返回名称为key的list中start至end之间的元素（下标从0开始，下同）
    ltrim(key, start, end)：截取名称为key的list，保留start至end之间的元素
    lindex(key, index)：返回名称为key的list中index位置的元素
    lset(key, index, value)：给名称为key的list中index位置的元素赋值为value
    lrem(key, count, value)：删除count个名称为key的list中值为value的元素。count为0，删除所有值为value的元素，count>0从 头至尾删除count个值为value的元素，count<0从尾到头删除|count|个值为value的元素。 
    lpop(key)：返回并删除名称为key的list中的首元素
    rpop(key)：返回并删除名称为key的list中的尾元素
    
    blpop(key1, key2,… key N, timeout)：lpop命令的block版本。
    即当timeout为0时，若遇到名称为key i的list不存在或该list为空，则命令结束。
    如果timeout>0，则遇到上述情况时，等待timeout秒，
    如果问题没有解决，则对 keyi+1开始的list执行pop操作。
    brpop(key1, key2,… key N, timeout)：rpop的block版本。参考上一命令。
    rpoplpush(srckey, dstkey)：返回并删除名称为srckey的list的尾元素，并将该元素添加到名称为dstkey的list的头部

5、对Set操作的命令
    sadd(key, member)：向名称为key的set中添加元素member
    srem(key, member) ：删除名称为key的set中的元素member
    spop(key) ：随机返回并删除名称为key的set中一个元素
    smove(srckey, dstkey, member) ：将member元素从名称为srckey的集合移到名称为dstkey的集合
    scard(key) ：返回名称为key的set的基数
    sismember(key, member) ：测试member是否是名称为key的set的元素
    sinter(key1, key2,…key N) ：求交集
    sinterstore(dstkey, key1, key2,…key N) ：求交集并将交集保存到dstkey的集合
    sunion(key1, key2,…key N) ：求并集
    sunionstore(dstkey, key1, key2,…key N) ：求并集并将并集保存到dstkey的集合
    sdiff(key1, key2,…key N) ：求差集
    sdiffstore(dstkey, key1, key2,…key N) ：求差集并将差集保存到dstkey的集合
    smembers(key) ：返回名称为key的set的所有元素
    srandmember(key) ：随机返回名称为key的set的一个元素

6、对zset（sorted set）操作的命令
    zadd(key, score, member)：向名称为key的zset中添加元素member，score用于排序。如果该元素已经存在，则根据score更新该元素的顺序。
    zrem(key, member) ：删除名称为key的zset中的元素member
    zincrby(key, increment, member) ：如果在名称为key的zset中已经存在元素member，则该元素的score增加increment；否则向集合中添加该元素，其score的值为increment
    zrank(key, member) ：返回名称为key的zset（元素已按score从小到大排序）中member元素的rank（即index，从0开始），若没有member元素，返回“nil”
    zrevrank(key, member) ：返回名称为key的zset（元素已按score从大到小排序）中member元素的rank（即index，从0开始），若没有member元素，返回“nil”
    zrange(key, start, end)：返回名称为key的zset（元素已按score从小到大排序）中的index从start到end的所有元素
    zrevrange(key, start, end)：返回名称为key的zset（元素已按score从大到小排序）中的index从start到end的所有元素
    zrangebyscore(key, min, max)：返回名称为key的zset中score >= min且score <= max的所有元素 
    zcard(key)：返回名称为key的zset的基数 
    zscore(key, element)：返回名称为key的zset中元素element的score 	
    zremrangebyrank(key, min, max)：删除名称为key的zset中rank >= min且rank <= max的所有元素 
    zremrangebyscore(key, min, max) ：删除名称为key的zset中score >= min且score <= max的所有元素
    zunionstore / zinterstore(dstkeyN, key1,…,keyN, WEIGHTS w1,…wN, AGGREGATE SUM|MIN|MAX)：对N个zset求并集和交集，并将最后的集合保存在dstkeyN中。对于集合中每一个元素的score，在进行 AGGREGATE运算前，都要乘以对于的WEIGHT参数。如果没有提供WEIGHT，默认为1。默认的AGGREGATE是SUM，即结果集合中元素 的score是所有集合对应元素进行SUM运算的值，而MIN和MAX是指，结果集合中元素的score是所有集合对应元素中最小值和最大值。

7、对Hash操作的命令
    hset(key, field, value)：向名称为key的hash中添加元素field<—>value
    hget(key, field)：返回名称为key的hash中field对应的value
    hmget(key, field1, …,field N)：返回名称为key的hash中field i对应的value
    hmset(key, field1, value1,…,field N, value N)：向名称为key的hash中添加元素field i<—>value i
    hincrby(key, field, integer)：将名称为key的hash中field的value增加integer
    hexists(key, field)：名称为key的hash中是否存在键为field的域
    hdel(key, field)：删除名称为key的hash中键为field的域
    hlen(key)：返回名称为key的hash中元素个数
    hkeys(key)：返回名称为key的hash中所有键
    hvals(key)：返回名称为key的hash中所有键对应的value
    hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value

8、持久化
    save：将数据同步保存到磁盘
    bgsave：将数据异步保存到磁盘
    lastsave：返回上次成功将数据保存到磁盘的Unix时戳
    shundown：将数据同步保存到磁盘，然后关闭服务

9、远程服务控制
    info：提供服务器的信息和统计
    monitor：实时转储收到的请求
    slaveof：改变复制策略设置
    config：在运行时配置Redis服务器
```

