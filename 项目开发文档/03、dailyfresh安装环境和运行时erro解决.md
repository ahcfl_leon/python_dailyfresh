[toc]

##### 1、安装Pillow  ERROR: Failed building wheel for Pillow

```shell
  Building wheel for Pillow (setup.py) ... error
  ERROR: Command errored out with exit status 1:
  command: /home/leonchen/.virtualenvs/django_df_py3/bin/python -u -c 'import sys, setuptools, tokenize; sys.argv[0] = '"'"'/tmp/pip-install-wyhyytb3/pillow_d738b82750384066a8080dd2675f3382/setup.py'"'"'; __file__='"'"'/tmp/pip-install-wyhyytb3/pillow_d738b82750384066a8080dd2675f3382/setup.py'"'"';f=getattr(tokenize, '"'"'open'"'"', open)(__file__);code=f.read().replace('"'"'\r\n'"'"', '"'"'\n'"'"');f.close();exec(compile(code, __file__, '"'"'exec'"'"'))' bdist_wheel -d /tmp/pip-wheel-q9s9tmvy 
  
ValueError: jpeg is required unless explicitly disabled using --disable-jpeg, aborting
  ----------------------------------------
  ERROR: Failed building wheel for Pillow
  Running setup.py clean for Pillow
Failed to build Pillow
Installing collected packages: Pillow
    Running setup.py install for Pillow ... error
    ERROR: Command errored out with exit status 1:
```

**解决办法是安装完整的依赖**

```shell
sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev
```

##### 2、安装MySQL-python时报错`EnvironmentError: mysql_config not found`的解决办法

1）、通过如下命令安装依赖关系

```
sudo apt-get install libmysqlclient-dev
```

2）、进入`mysql_config`文件所目录路径，执行如下两条命令

​	  其中`mysql_config`文件的默认路径是在`/usr/bin/mysql_config`。

```
sudo updatedb
locate mysql_config
```

##### 3、`python`使用`FastDFS`客户端上传文件，实例化报错：ModuleNotFoundError: No module named 'mutagen._compat'

`python`使用`FastDFS`客户端上传文件，实例化报错：`ModuleNotFoundError: No module named 'mutagen._compat'`，奇怪了，直接用的第三方包怎么还有错呢，一番发现了问题。

`mutagen`的安装包内查找,发现原来`_compat`文件是在`_senf`目录下,而在`FastDFS`客户端里用的直接是`mutagen._compat`,怪不得提示找不到模块，我们重新修改下路径

```python
# 修改导入路径
from mutagen._senf._compat import StringIO
```

##### 4、Python使用 Django 显示不出静态页面："GET /static/main/css/bootstrap.min.css.map HTTP/1.1" 404

```shell
# 需要在setting.py中加入，因为启动配置项找不到静态文件夹
STATICFILES_DIRS = [os.path.join(BASE_DIR,‘static’]
```

##### 5、RemovedInDjango19Warning: 'get_cache' is deprecated in favor of 'caches'. cache = get_cache(alias)

>django版本问题 

##### 6、NoOptionError at /admin/goods/indexpromotionbanner/2/   -> No option 'connect_timeout' in section: '__config__'

> fdfs-client-py与python3不兼容，在传入文件超时， 
>
>client_conf = settings.FDFS_CLIENT_CONF 
>
>在读取配置文件时，因为获得不到不到cilent.conf的路径

**解决**：安装py3Fdfs

```shell
pip uninstall fdfs-client-py
pip install py3Fdfs
```

>改为使用get_tracker_conf()方法传入cilent.conf的路径，
>这个方法内部在读取的时候，最终调用的是utils.py文件中的read方法，
>而read方法中有个判断条件是判断我们传入的文件名是否为basestring类型的，
>
>但basestring类型是python2中的数据类型，在python 3解释器中会报错，所以这里会继续报错。

**解决**：
在保存文件的时候，它会返回一个字典，里面包含一个文件名，这个我们要返回去保存的，但它的类型是bytes，需要解码，加上decode()。

return filename.decode()

##### 7、支付宝接口跳转：http://127.0.0.1:8000/order/pay  FileNotFoundError at /order/pay

##### [Errno 2] No such file or directory:'/home/cfl/bj18/item/dailyfresh/apps/order/app_private_key.pem'

```shell
Exception Type: FileNotFoundError at /order/pay
Exception Value: [Errno 2] No such file or directory: '/home/cfl/bj18/item/dailyfresh/apps/order/app_private_key.pem'
Request information:
GET: No GET data
# 公钥路径错误  
```

##### 8、Celery生成静态页面：django.core.urlresolvers.NoReverseMatch: Reverse for 'order' with arguments '()' and keyword arguments '{}' not found. 1 pattern(s) tried: ['user/order/(?P<page>\\d+)$']

File "/home/leonchen/.virtualenvs/django_df_py3/lib/python3.6/site-packages/django/core/urlresolvers.py", line 496, in _reverse_with_prefix
    (lookup_view_s, args, kwargs, len(patterns), patterns))

##### 9、ERROR: THESE PACKAGES DO NOT MATCH THE HASHES FROM THE REQUIREMENTS FILE. 

If you have updated the package versions, please update the hashes. Otherwise, examine the package contents carefully; someone may have tampered with them.

```shell
pip install django==1.8.2
# 报错
ERROR: THESE PACKAGES DO NOT MATCH THE HASHES FROM THE REQUIREMENTS FILE
-------------------------------------------------------------------------------------
1、原因：可能是因为网速问题导致的下载错误，导致对应的哈希值不匹配。 
# 解决
pip install --upgrade --default-timeout=100000 packagename -i http://pypi.douban.com/simple
2、当用pip安装的时候，pip会首先检查安装包里的pip cache，如果安装包找到了，而且是最新的，pip就会抓取并且安装安装包里的 .whl 文件，这个就会生成 badzipfile，也肯导致 “do not match hash”
# 解决
pip install --no-cache-dir django==1.1
```

```shell
# 分析下我的原因
# 因为我之前项目不兼容有修改了python3.6/site-packages/Django包内容，可能导致对应的哈希值不匹配
# 试着直接使用源地址，可以 raise error
pip install https://pypi.tuna.tsinghua.edu.cn/packages/4e/9d/2a1835ccbf8e1f0d6755d0e938ffd855f23886d055a7a18cc00a5224a99b/Django-1.8.2-py2.py3-none-any.whl
# 接着报错
Rolling back uninstall of Django
  Moving to /home/leonchen/.virtualenvs/django_df_py3/lib/python3.6/site-packages/django/
   from /home/leonchen/.virtualenvs/django_df_py3/lib/python3.6/site-packages/~jango
ERROR: Exception:

File "/usr/lib/python3.6/zipfile.py", line 962, in _read1
    self._update_crc(data)
  File "/usr/lib/python3.6/zipfile.py", line 890, in _update_crc
    raise BadZipFile("Bad CRC-32 for file %r" % self.name)
zipfile.BadZipFile: Bad CRC-32 for file 'django/db/models/sql/subqueries.py'
# 下面是解压缩协议的预期行为。有时一个无效的密码会通过密码检查并被CRC检查捕获。来自man unzip（一个Linux文档，但算法与平台无关）：
    The correct password will always check out against the header, but there is a 1-in-256 chance that an incorrect password will as well. (This is a security feature of the PKWARE zipfile format; it helps prevent brute-force attacks that might otherwise gain a large speed advantage by testing only the header.) In the case that an incorrect password is given but it passes the header test anyway, either an incorrect CRC will be generated for the extracted data or else unzip will fail during the extraction because the ``decrypted'' bytes do not constitute a valid compressed data stream.
# 解决：
--no-cache-dir django  # 不使用django1.8.2的缓存文件下载 
```

##### 10、从windows系统拷贝到linux系统pycharm中时经常会多一些空字符串，source code string cannot contain null bytes” 

导致运行python脚本报错，“source code string cannot contain null bytes” 
 而pycharm中是无法察觉空字符串的（哪位朋友有解决办法，麻烦高诉我一下，不胜感激） 
 用vi打开文件才能看出来，多了很多“^@”,即空字符(ascii 码 0，在程序里一般写作”\0”)，在 vim 里就显示成 ^@。 
 替换命令

```
:%s/\%x00//g
```