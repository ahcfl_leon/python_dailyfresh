# 项目部署

## 0、wsgi和uwsgi

#### 1.WSGI

- 在生产环境中使用WSGI作为python web的服务器
- WSGI：全拼为Python Web Server Gateway Interface，Python  Web服务器网关接口，是Python应用程序或框架和Web服务器之间的一种接口，被广泛接受。WSGI没有官方的实现,  因为WSGI更像一个协议，只要遵照这些协议，WSGI应用(Application)都可以在任何服务器(Server)上运行
- 项目默认会生成一个wsgi.py文件，确定了settings模块、application对象
  - application对象：在Python模块中使用application对象与应用服务器交互
  - settings模块：用于进行项目配置

#### 2.uWSGI

- uWSGI实现了WSGI的所有接口，是一个快速、自我修复、开发人员和系统管理员友好的服务器
- uWSGI代码完全用C编写，效率高、性能稳定
- 安装uWSGI

## 1 、uwsgi作为web服务器

遵循wsgi协议的web服务器。

#### 1.1 uwsgi的安装

​	pip install uwsgi

#### 1.2 uwsgi的配置

* 项目部署时，需要设置settings.py文件夹下的：

  DEBUG=FALSE

  ALLOWED_HOSTS=[‘*’] 

* 在项目下创建uwsgi.ini配置文件，并做以下配置

```
[uwsgi]
#使用nginx连接时使用
#socket=127.0.0.1:8080
#直接做web服务器使用  python manage.py runserver ip:port
http=127.0.0.1:8080
#项目目录
chdir=/home/cfl/bj18/item/dailyfresh
#项目中wsgi.py文件的目录，相对于项目目录
wsgi-file=dailyfresh/wsgi.py
#指定启动的工作进程数
processes=4
#指定工作进程中的线程数
threads=2
#主进程
master=True
#保存启动之后主进程的pid
pidfile=uwsgi.pid
#设置uwsgi后台运行, uwsgi.log保存日志信息
daemonize=uwsgi.log
#设置虚拟环境的路径
virtualenv=/home/cfl/.virtualenvs/py3_django
```

#### 1.3 uwsgi的启动和停止

```
启动:uwsgi –-ini 配置文件路径 		 例如:uwsgi –-ini uwsgi.ini

停止:uwsgi --stop uwsgi.pid路径 	例如:uwsgi –-stop uwsgi.pid

tail -f uwsgi.log
```

当启动时我们访问网站，不能显示静态文件样式。

因为我们DEBUG=True是，django对静态文件进行处理，当改为False时，django不能进行处理。

那么就需要使用nginx来提供静态文件。

## 2 、nginx

#### 2.1 nginx 配置转发请求给uwsgi  

* cd /usr/local/nginx/cnf   配置 nginx.conf 文件

```
# 例如:
location / {

	include uwsgi_params;

	uwsgi_pass uwsgi服务器的ip:port;

}
-------------------------------------------------------
cfl@cfl-virtual-machine:~/bj18/dailyfresh$ sudo mkdir -p /var/www/dailyfresh/static
[sudo] cfl 的密码： 
cfl@cfl-virtual-machine:~/bj18/dailyfresh$ cd /var
cfl@cfl-virtual-machine:/var$ ls
backups  cache  crash  lib  local  lock  log  mail  metrics  opt  run  snap  spool  tmp  www
cfl@cfl-virtual-machine:/var$ cd www/dailyfresh/static/
cfl@cfl-virtual-machine:/var/www/dailyfresh/static$ ls
cfl@cfl-virtual-machine:/var/www/dailyfresh/static$ pwd
/var/www/dailyfresh/static
cfl@cfl-virtual-machine:/var/www/dailyfresh/static$ 
--------------------------------------------------------
location / {
	# 包含uwsgi的请求参数
	include uwsgi_params;
	# 转交请求给uwsgi
	uwsgi_pass 127.0.0.1:8080;
}

location /static {
# 指定静态文件存放的目录
	alias /var/www/dailyfresh/static;

}
```

#### 2.2 nginx配置处理静态文件

```shell
django settings.py中配置收集静态文件路径:

STATIC_ROOT=收集的静态文件路径 例如:/var/www/dailyfresh/static;

终端执行：sudo chmod 777 /var/www/dailyfresh/static/
-----------------------------------------------------------------
django 收集静态文件的命令:

python manage.py collectstatic

执行上面的命令会把项目中所使用的静态文件收集到STATIC_ROOT指定的目录下。
------------------------------------------------------------------
收集完静态文件之后,让nginx提供静态文件，需要在nginx配置文件中增加如下配置:

location /static {

 	alias /var/www/dailyfresh/static/;

}
```

重启 sudo ./nginx -s reload

测试访问网站   (访问动态页面)127.0.0.1/       (访问静态页面)127.0.0.1/static

#### 2.3 nginx转发请求给另外地址  （调度另外一个nginx+celery）

在location 对应的配置项中增加 proxy_pass 转发的服务器地址。

如当用户访问127.0.0.1时，在nginx中配置把这个请求转发给172.16.179.131:80(nginx)服务器，让这台服务器提供静态首页。

配置如下:

```
location = /{

	proxy_pass http://172.16.179.131;
}
```

9.2.4 [nginx配置upstream实现负载均衡](http://www.baidu.com/link?url=tRw5xlP0ktNnJtBzk3YU6DNaWgD6zFGKamwpc8k4qastl1_eDyHAJH4sj0FSRsvtsd7k_wrl8PX7S_YGd7XpY_)

ngnix 配置负载均衡时，在server配置的前面增加upstream配置项。

```
增加配置项：
upstream dailyfresh {

	server 127.0.0.1:8080;

	server 127.0.0.1:8081;

}

location / {
# 包含uwsgi的请求参数
include uwsgi_params;
# 转交请求给uwsgi
#uwsgi_pass 127.0.0.1:8080;
uwsgi_pass dailyfresh;
}
```



#### 2.4 部署项目流程图

```
----------fdfs存储图片---------------
server {
    listen 8888;
    server_name localhost;
    location ~/group[0-9]/ {
    ngx_fastdfs_module;
}
    error_page 500 502 503 504  /50x.html;
    location = /50x.html {
    root html;
    }
}
------------- celery生成静态页面-------------------------
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;

    #access_log  logs/host.access.log  main;

    location /static {
        alias /home/cfl/bj18/dailyfresh/static/;
    }


    location / {
    	#root   html;
       root /home/cfl/bj18/dailyfresh/static;
       index  index.html index.htm;
     
	}
-------------------- Nginx服务器部署调度动态/静态页面请求--------------------------
location / {
    # 包含uwsgi的请求参数
    include uwsgi_params;
    # 转交请求给uwsgi
    uwsgi_pass 127.0.0.1:8080;
}

location /static {
    # 指定静态文件存放的目录
    alias /var/www/dailyfresh/static;

}

location = / {
    # 传递请求给静态文件服务器的nginx
    proxy_pass http://192.168.182.137;

}

```

