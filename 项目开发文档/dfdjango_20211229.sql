-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: dailyfresh_django_py3
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_20f85452c62a60d1_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_20f85452c62a60d1_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_22dc0eb08ad83fe0_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth__content_type_id_59bffdc2f30c1f9f_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add 用户',6,'add_user'),(17,'Can change 用户',6,'change_user'),(18,'Can delete 用户',6,'delete_user'),(19,'Can add 地址',7,'add_address'),(20,'Can change 地址',7,'change_address'),(21,'Can delete 地址',7,'delete_address'),(22,'Can add 商品种类',8,'add_goodstype'),(23,'Can change 商品种类',8,'change_goodstype'),(24,'Can delete 商品种类',8,'delete_goodstype'),(25,'Can add 商品',9,'add_goodssku'),(26,'Can change 商品',9,'change_goodssku'),(27,'Can delete 商品',9,'delete_goodssku'),(28,'Can add 商品SPU',10,'add_goods'),(29,'Can change 商品SPU',10,'change_goods'),(30,'Can delete 商品SPU',10,'delete_goods'),(31,'Can add 商品图片',11,'add_goodsimage'),(32,'Can change 商品图片',11,'change_goodsimage'),(33,'Can delete 商品图片',11,'delete_goodsimage'),(34,'Can add 首页轮播商品',12,'add_indexgoodsbanner'),(35,'Can change 首页轮播商品',12,'change_indexgoodsbanner'),(36,'Can delete 首页轮播商品',12,'delete_indexgoodsbanner'),(37,'Can add 主页分类展示商品',13,'add_indextypegoodsbanner'),(38,'Can change 主页分类展示商品',13,'change_indextypegoodsbanner'),(39,'Can delete 主页分类展示商品',13,'delete_indextypegoodsbanner'),(40,'Can add 主页促销活动',14,'add_indexpromotionbanner'),(41,'Can change 主页促销活动',14,'change_indexpromotionbanner'),(42,'Can delete 主页促销活动',14,'delete_indexpromotionbanner'),(43,'Can add 订单',15,'add_orderinfo'),(44,'Can change 订单',15,'change_orderinfo'),(45,'Can delete 订单',15,'delete_orderinfo'),(46,'Can add 订单商品',16,'add_ordergoods'),(47,'Can change 订单商品',16,'change_ordergoods'),(48,'Can delete 订单商品',16,'delete_ordergoods');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_address`
--

DROP TABLE IF EXISTS `df_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `receiver` varchar(20) NOT NULL,
  `addr` varchar(256) NOT NULL,
  `zip_code` varchar(6) DEFAULT NULL,
  `phone` varchar(11) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_address_user_id_670f9bc8052710b_fk_df_user_id` (`user_id`),
  CONSTRAINT `df_address_user_id_670f9bc8052710b_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_address`
--

LOCK TABLES `df_address` WRITE;
/*!40000 ALTER TABLE `df_address` DISABLE KEYS */;
INSERT INTO `df_address` VALUES (1,'2021-01-28 03:22:44.844989','2021-01-28 03:22:44.845026',0,'上官婉儿','北京市朝阳区','10001','13252052011',1,1);
/*!40000 ALTER TABLE `df_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_goods`
--

DROP TABLE IF EXISTS `df_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `detail` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_goods`
--

LOCK TABLES `df_goods` WRITE;
/*!40000 ALTER TABLE `df_goods` DISABLE KEYS */;
INSERT INTO `df_goods` VALUES (1,'2017-11-15 03:03:05.257969','2017-11-15 03:03:05.258130',0,'草莓','<p><strong>很不错的草莓</strong></p>'),(2,'2017-11-15 03:05:36.964951','2017-11-15 03:05:36.965129',0,'葡萄',''),(3,'2017-11-15 03:05:52.323866','2017-11-15 03:05:52.323949',0,'柠檬',''),(4,'2017-11-15 03:06:01.267481','2017-11-15 03:06:01.267615',0,'奇异果',''),(5,'2017-11-15 03:06:30.418683','2017-11-15 03:06:30.418789',0,'大青虾',''),(6,'2017-11-15 03:06:35.994464','2017-11-15 03:06:35.994567',0,'秋刀鱼',''),(7,'2017-11-15 03:06:48.115318','2017-11-15 03:06:48.115410',0,'扇贝',''),(8,'2017-11-15 03:07:03.057514','2017-11-15 03:07:03.057601',0,'基围虾',''),(9,'2017-11-15 03:07:36.306725','2017-11-15 03:07:36.306926',0,'猪肉',''),(10,'2017-11-15 03:07:39.056064','2017-11-15 03:07:39.056145',0,'牛肉',''),(11,'2017-11-15 03:07:41.955755','2017-11-15 03:07:41.955833',0,'羊肉',''),(12,'2017-11-15 03:07:44.741474','2017-11-15 03:07:44.741574',0,'牛排',''),(13,'2017-11-15 03:07:51.748699','2017-11-15 03:07:51.748828',0,'鸡蛋',''),(14,'2017-11-15 03:07:56.413773','2017-11-15 03:07:56.413853',0,'鸡肉',''),(15,'2017-11-15 03:07:59.568405','2017-11-15 03:07:59.568554',0,'鸭蛋',''),(16,'2017-11-15 03:08:03.020608','2017-11-15 03:08:03.020764',0,'鸡腿',''),(17,'2017-11-15 03:08:10.063820','2017-11-15 03:08:10.063898',0,'白菜',''),(18,'2017-11-15 03:08:13.315906','2017-11-15 03:08:13.316025',0,'芹菜',''),(19,'2017-11-15 03:08:16.351445','2017-11-15 03:08:16.351526',0,'香菜',''),(20,'2017-11-15 03:08:24.232660','2017-11-15 03:08:24.232743',0,'冬瓜',''),(21,'2017-11-15 03:08:36.939678','2017-11-15 03:08:36.940113',0,'鱼丸',''),(22,'2017-11-15 03:08:43.194862','2017-11-15 03:08:43.194985',0,'蟹棒',''),(23,'2017-11-15 03:08:50.771785','2017-11-15 03:08:50.771931',0,'虾丸',''),(24,'2017-11-15 03:09:01.546052','2017-11-15 03:09:01.546152',0,'速冻水饺',''),(25,'2017-11-14 08:50:50.383071','2017-11-14 08:50:50.383115',0,'芒果',''),(26,'2017-11-17 07:54:26.657410','2017-11-17 07:54:26.657443',0,'鹌鹑蛋',''),(27,'2017-11-17 07:54:35.205668','2017-11-17 07:54:35.205703',0,'鹅蛋',''),(28,'2017-11-17 07:54:46.756236','2017-11-17 07:54:46.756272',0,'红辣椒','');
/*!40000 ALTER TABLE `df_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_goods_image`
--

DROP TABLE IF EXISTS `df_goods_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_goods_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `image` varchar(100) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_goods_image_22ad5bca` (`sku_id`),
  CONSTRAINT `df_goods_image_sku_id_57d13779fa9a3376_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_goods_image`
--

LOCK TABLES `df_goods_image` WRITE;
/*!40000 ALTER TABLE `df_goods_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `df_goods_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_goods_sku`
--

DROP TABLE IF EXISTS `df_goods_sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_goods_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unite` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `sales` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_goods_sku_goods_id_d9cdaf18eaacb1f_fk_df_goods_id` (`goods_id`),
  KEY `df_goods_sku_94757cae` (`type_id`),
  CONSTRAINT `df_goods_sku_goods_id_d9cdaf18eaacb1f_fk_df_goods_id` FOREIGN KEY (`goods_id`) REFERENCES `df_goods` (`id`),
  CONSTRAINT `df_goods_sku_type_id_645abd8db8a23b0e_fk_df_goods_type_id` FOREIGN KEY (`type_id`) REFERENCES `df_goods_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_goods_sku`
--

LOCK TABLES `df_goods_sku` WRITE;
/*!40000 ALTER TABLE `df_goods_sku` DISABLE KEYS */;
INSERT INTO `df_goods_sku` VALUES (1,'2017-11-15 03:10:14.045538','2017-11-14 08:24:49.138489',0,'长丰红草莓','草莓简介',10.00,'500g','group1/M00/00/00/wKi2iWASgAGAbAzpAAAgrKNKuOg123.jpg',96,2,1,1,1),(2,'2017-11-15 03:11:04.490384','2017-11-14 08:44:43.484243',0,'盒装草莓','草莓简介',20.00,'盒','group1/M00/00/00/wKi2iWASgcmATWGWAADhpU9_Ylo566.jpg',9,1,1,1,1),(3,'2017-11-15 03:12:32.165020','2017-11-14 08:25:22.505620',0,'新疆大葡萄','葡萄简介',20.00,'500g','group1/M00/00/00/wKi2iWASg46AK4r8AAAjjiYTEkw240.jpg',7,0,1,2,1),(4,'2017-11-15 03:13:16.457844','2017-11-14 08:25:34.181904',0,'广西武鸣沃柑','简介',32.00,'500g','group1/M00/00/00/wKi2iWATiwOAKzU3AAIWSG0MoPQ362.jpg',12,0,1,3,1),(5,'2017-11-15 03:14:05.799352','2017-11-14 08:25:56.427676',0,'四川猕猴桃','简介',12.12,'500g','group1/M00/00/00/wKi2iWATiv6AffTdAANqjiY0BME475.jpg',12,0,1,4,1),(6,'2017-11-15 03:15:09.971968','2017-11-14 08:26:09.113586',0,'巢湖淡水虾','简介',34.00,'500g','group1/M00/00/00/wKi2iWATiuKAGiKUAA4YLYpafmg384.png',12,0,1,5,2),(7,'2017-11-15 03:15:53.812181','2017-11-14 08:26:19.094675',0,'北海道秋刀鱼','简介',50.00,'500g','group1/M00/00/00/wKi2iWAShFmAd-BPAAAkaP_7_18945.jpg',15,0,1,6,2),(8,'2017-11-15 03:16:24.763232','2017-11-14 08:26:31.121824',0,'脆口鱿鱼须','简介',56.60,'500g','group1/M00/00/00/wKi2iWATj6yAW4nQAAKK-K_g1dI776.jpg',13,0,1,7,2),(9,'2017-11-15 03:17:13.426611','2017-11-14 08:26:58.739624',0,'速冻小白鲳','简介',100.90,'500g','group1/M00/00/00/wKi2iWATj7OAX7HqAAHRfOqkC5s373.jpg',14,0,1,8,2),(10,'2017-11-15 03:17:47.656066','2017-11-14 08:29:56.158261',0,'雨润猪肉','简介',23.99,'500g','group1/M00/00/00/wKi2iWATivmAPxwfAAE4m1odU3w526.png',100,0,1,9,3),(11,'2017-11-15 03:18:15.497630','2017-11-14 08:31:27.169999',0,'牛肉','简介',34.99,'500g','group1/M00/00/00/rBCzg1oKqd-AUsoBAAEExAU4yXU2908730',100,0,1,10,3),(12,'2017-11-15 03:18:44.453933','2017-11-14 08:32:22.493340',0,'羊肉','简介',56.99,'500g','group1/M00/00/00/rBCzg1oKqhaAKgwkAAB6NOQDrpk3374052',100,0,1,11,3),(13,'2017-11-15 03:19:10.209472','2017-11-14 08:33:15.061544',0,'蒙城牛肉','简介',99.99,'500g','group1/M00/00/00/wKi2iWAShdKAQSbyAACwa3rCDPQ106.jpg',100,89,1,12,3),(14,'2017-11-15 03:19:44.020204','2017-11-14 08:34:31.275370',0,'陵川土鸡蛋','简介',23.00,'500g','group1/M00/00/00/wKi2iWASiUKAOPSLAADUKbwLSqY649.jpg',100,0,1,13,4),(15,'2017-11-15 03:20:20.962831','2017-11-14 08:35:21.725162',0,'鸡肉','简介',32.00,'500g','group1/M00/00/00/rBCzg1oKqsmAVxzcAADUY5hC_sI5143658',100,0,1,14,4),(16,'2017-11-15 03:20:53.724305','2017-11-14 08:37:27.336911',0,'鸭蛋','简介',45.00,'盒','group1/M00/00/00/rBCzg1oKq0eAMxKFAAFC_2tSkFo4950479',121,0,1,15,4),(17,'2017-11-15 03:21:22.965398','2017-11-14 08:38:08.440778',0,'鸡腿','简介',45.00,'500g','group1/M00/00/00/rBCzg1oKq3CADiewAAA2_p7G96w3860045',12,0,1,16,4),(18,'2017-11-15 03:22:04.462490','2017-11-14 08:38:45.119926',0,'白菜','简介',4.50,'500g','group1/M00/00/00/wKi2iWASjBCAKrJkAADWHYeKaNI130.jpg',100,0,1,17,5),(19,'2017-11-15 03:22:31.745392','2017-11-14 08:39:40.030728',0,'利辛有机韭菜','简介',3.50,'500g','group1/M00/00/00/wKi2iWATivCAD5VsAAKdXewZr_M937.jpg',12,0,1,18,5),(20,'2017-11-15 03:23:21.161526','2017-11-14 08:40:08.185684',0,'有机豌豆荚','简介',7.90,'500g','group1/M00/00/00/wKi2iWATib2ABuLeAAFr4PvGAPY828.jpg',100,0,1,19,5),(21,'2017-11-15 03:23:46.986158','2017-11-14 08:40:38.330247',0,'冬瓜','简介',12.99,'500g','group1/M00/00/00/rBCzg1oKrAaAN1Z6AAENHrNG1-s8874196',100,0,1,20,5),(22,'2017-11-15 03:24:10.445214','2017-11-14 08:41:19.155821',0,'鱼丸','简介',66.00,'500g','group1/M00/00/00/rBCzg1oKrC-ACdOBAADZQphQJ2o3748807',12,0,1,21,6),(23,'2017-11-15 03:24:37.927158','2017-11-14 08:41:59.658787',0,'蟹棒','简介',68.00,'500g','group1/M00/00/00/rBCzg1oKrFeAJ9PuAABxy5vKkgY2006901',100,0,1,22,6),(24,'2017-11-15 03:25:18.235816','2017-11-14 08:42:25.868409',0,'虾丸','简介',89.99,'500g','group1/M00/00/00/rBCzg1oKrHGADF7jAABICav_wjk1418828',100,0,1,23,6),(25,'2017-11-15 03:25:56.170531','2017-11-14 08:43:18.768380',0,'速冻水饺','简介',20.00,'袋','group1/M00/00/00/wKi2iWASjE-AGbBcAACMoBJXjDs117.jpg',100,0,1,24,6),(26,'2017-11-14 08:53:00.188619','2017-11-14 08:53:00.188652',0,'越南芒果','新鲜越南芒果',29.90,'2.5kg','group1/M00/00/00/rBCzg1oKruyABIIzAAByzTJcTjM7085820',100,0,1,25,1),(27,'2017-11-17 07:57:00.677981','2017-11-17 07:57:00.678022',0,'鹌鹑蛋','简介',39.80,'126枚','group1/M00/00/00/rBCzg1oOlkyAHiH3AAGZ6KapWiA5556935',100,0,1,26,4),(28,'2017-11-17 07:58:18.361078','2017-11-17 07:58:18.361122',0,'鹅蛋','简介',49.90,'6枚','group1/M00/00/00/rBCzg1oOlpqAOZ8gAADg_NUp5b47679136',80,0,1,27,4),(29,'2017-11-17 07:59:48.998394','2017-11-17 07:59:48.998431',0,'成都红辣椒','简介',11.00,'2.5kg','group1/M00/00/00/wKi2iWASiGmAOgxMAAHXO8pdocY043.jpg',150,0,1,28,5);
/*!40000 ALTER TABLE `df_goods_sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_goods_type`
--

DROP TABLE IF EXISTS `df_goods_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_goods_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `logo` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_goods_type`
--

LOCK TABLES `df_goods_type` WRITE;
/*!40000 ALTER TABLE `df_goods_type` DISABLE KEYS */;
INSERT INTO `df_goods_type` VALUES (1,'2017-11-14 05:02:09.888544','2017-11-14 05:02:09.888598',0,'新鲜水果','fruit','group1/M00/00/00/wKi2iWASTrCAaUN7AAAmv27pX4k5128220'),(2,'2017-11-14 05:04:32.069517','2017-11-14 05:04:32.069561',0,'海鲜水产','seafood','group1/M00/00/00/wKi2iWASTr2ABG5gAABHr3RQqFs2434144'),(3,'2017-11-14 05:05:34.514415','2017-11-14 05:05:34.514449',0,'猪牛羊肉','meet','group1/M00/00/00/wKi2iWASTtGAKlKzAAAy1Tlm9So4489479'),(4,'2017-11-14 05:05:58.366135','2017-11-14 05:05:58.366170',0,'禽类蛋品','egg','group1/M00/00/00/wKi2iWASTuGAbXuYAAAqR4DoSUg5258171'),(5,'2017-11-14 05:06:32.561861','2017-11-14 05:06:32.561895',0,'新鲜蔬菜','vegetables','group1/M00/00/00/wKi2iWASTuuARTtBAAA-0ZoYkpM6390704'),(6,'2017-11-14 05:06:55.562634','2017-11-14 05:06:55.562673',0,'速冻食品','ice','group1/M00/00/00/wKi2iWASTvSAAyIFAAA3sZPrVzQ1310257');
/*!40000 ALTER TABLE `df_goods_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_index_banner`
--

DROP TABLE IF EXISTS `df_index_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_index_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `image` varchar(100) NOT NULL,
  `index` smallint(6) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_index_banner_sku_id_ca9fcc3b7130c14_fk_df_goods_sku_id` (`sku_id`),
  CONSTRAINT `df_index_banner_sku_id_ca9fcc3b7130c14_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_index_banner`
--

LOCK TABLES `df_index_banner` WRITE;
/*!40000 ALTER TABLE `df_index_banner` DISABLE KEYS */;
INSERT INTO `df_index_banner` VALUES (1,'2017-11-14 08:48:05.549864','2017-11-14 08:48:05.549896',0,'group1/M00/00/00/wKi2iWASVjKAcbrfAACpB-LsCdE7294313',0,5),(2,'2017-11-14 08:53:26.498965','2017-11-14 08:53:26.499001',0,'group1/M00/00/00/wKi2iWASViOARLd7AAC3B-z8J2c1402438',1,26),(3,'2017-11-14 08:53:40.586457','2017-11-14 08:53:40.586490',0,'group1/M00/00/00/wKi2iWASVhiAHAjHAAETwXb_pso7837907',2,13),(4,'2017-11-14 08:54:02.805958','2017-11-14 08:54:02.805992',0,'group1/M00/00/00/wKi2iWASVg-ACwppAAD0akkXmFo5371198',3,9);
/*!40000 ALTER TABLE `df_index_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_index_promotion`
--

DROP TABLE IF EXISTS `df_index_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_index_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` varchar(256) NOT NULL,
  `image` varchar(100) NOT NULL,
  `index` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_index_promotion`
--

LOCK TABLES `df_index_promotion` WRITE;
/*!40000 ALTER TABLE `df_index_promotion` DISABLE KEYS */;
INSERT INTO `df_index_promotion` VALUES (1,'2017-11-14 08:56:21.863522','2017-11-17 08:29:08.554743',0,'吃货暑假趴','#','group1/M00/00/00/wKi2iWASTlGAGh4TAAA2pLUeB601916008',0),(2,'2017-11-14 08:56:53.522161','2017-11-14 08:56:53.522193',0,'盛夏尝鲜季','#','group1/M00/00/00/wKi2iWASTmSALoC2AAA98yvCs1I2059808',1);
/*!40000 ALTER TABLE `df_index_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_index_type_goods`
--

DROP TABLE IF EXISTS `df_index_type_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_index_type_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `display_type` smallint(6) NOT NULL,
  `index` smallint(6) NOT NULL,
  `sku_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_index_type_goods_sku_id_28116816c7e3d3de_fk_df_goods_sku_id` (`sku_id`),
  KEY `df_index_type_goods_type_id_26f847afb7e50885_fk_df_goods_type_id` (`type_id`),
  CONSTRAINT `df_index_type_goods_sku_id_28116816c7e3d3de_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`),
  CONSTRAINT `df_index_type_goods_type_id_26f847afb7e50885_fk_df_goods_type_id` FOREIGN KEY (`type_id`) REFERENCES `df_goods_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_index_type_goods`
--

LOCK TABLES `df_index_type_goods` WRITE;
/*!40000 ALTER TABLE `df_index_type_goods` DISABLE KEYS */;
INSERT INTO `df_index_type_goods` VALUES (1,'2017-11-14 08:57:41.509910','2017-11-14 08:57:41.509945',0,1,0,1,1),(2,'2017-11-14 08:57:50.129355','2017-11-14 08:57:50.129388',0,1,1,3,1),(3,'2017-11-14 08:58:00.896427','2017-11-14 08:58:00.896459',0,1,2,5,1),(4,'2017-11-14 08:58:20.417072','2017-11-14 08:58:20.417107',0,1,3,4,1),(5,'2017-11-14 08:58:32.934165','2017-11-14 08:58:32.934197',0,0,0,2,1),(6,'2017-11-14 08:58:53.943189','2017-11-14 08:58:53.943227',0,0,1,4,1),(7,'2017-11-14 08:59:16.396829','2017-11-14 08:59:16.396864',0,1,0,6,2),(8,'2017-11-14 08:59:25.723510','2017-11-14 08:59:25.723545',0,1,1,7,2),(9,'2017-11-14 08:59:37.353278','2017-11-14 08:59:37.353315',0,1,2,8,2),(10,'2017-11-14 08:59:48.082119','2017-11-14 09:30:28.117330',0,1,3,9,2),(11,'2017-11-14 08:59:59.725972','2017-11-14 08:59:59.726006',0,0,0,9,2),(12,'2017-11-14 09:00:11.685051','2017-11-14 09:00:11.685098',0,0,1,8,2),(13,'2017-11-14 09:00:20.409490','2017-11-14 09:00:20.409522',0,1,0,10,3),(15,'2017-11-14 09:00:41.325634','2017-11-14 09:00:41.325668',0,1,2,12,3),(16,'2017-11-14 09:00:56.193991','2017-11-14 09:00:56.194023',0,1,3,13,3),(17,'2017-11-14 09:01:09.550978','2017-11-14 09:01:09.551016',0,0,0,15,3),(18,'2017-11-14 09:01:18.798219','2017-11-14 09:01:18.798251',0,1,1,17,3),(19,'2017-11-14 09:01:29.182673','2017-11-14 09:01:29.182705',0,1,0,14,4),(20,'2017-11-14 09:01:44.702111','2017-11-14 09:01:44.702146',0,1,1,16,4),(21,'2017-11-14 09:02:01.490018','2017-11-14 09:02:01.490053',0,0,0,14,4),(22,'2017-11-14 09:02:14.000306','2017-11-14 09:02:14.000344',0,0,1,16,4),(23,'2017-11-14 09:02:29.300733','2017-11-14 09:02:29.300768',0,1,0,18,5),(24,'2017-11-14 09:02:38.655411','2017-11-14 09:02:38.655444',0,1,1,19,5),(25,'2017-11-14 09:02:48.641048','2017-11-14 09:02:48.641080',0,1,2,20,5),(26,'2017-11-14 09:03:01.896718','2017-11-14 09:03:01.896759',0,0,0,20,5),(27,'2017-11-14 09:03:14.583044','2017-11-14 09:03:14.583086',0,0,1,19,5),(28,'2017-11-14 09:03:27.597171','2017-11-14 09:03:27.597206',0,1,0,22,6),(29,'2017-11-14 09:03:37.078417','2017-11-14 09:03:37.078451',0,1,1,23,6),(30,'2017-11-14 09:03:48.459266','2017-11-14 09:03:48.459299',0,1,2,24,6),(31,'2017-11-14 09:03:58.834392','2017-11-14 09:03:58.834428',0,1,3,25,6),(32,'2017-11-14 09:04:11.118584','2017-11-14 09:04:11.118628',0,0,0,23,6),(33,'2017-11-14 09:04:21.235831','2017-11-14 09:04:21.235887',0,0,1,25,6),(34,'2017-11-17 08:00:09.522776','2017-11-17 08:00:09.522811',0,1,2,27,4),(35,'2017-11-17 08:00:19.382093','2017-11-17 08:00:19.382125',0,1,3,28,4),(36,'2017-11-17 08:00:31.352237','2017-11-17 08:00:31.352274',0,1,3,29,5);
/*!40000 ALTER TABLE `df_index_type_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_order_goods`
--

DROP TABLE IF EXISTS `df_order_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `count` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `comment` varchar(256) NOT NULL,
  `order_id` varchar(128) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_order_goods_69dfcb07` (`order_id`),
  KEY `df_order_goods_22ad5bca` (`sku_id`),
  CONSTRAINT `df_order_goo_order_id_174154055378a4a9_fk_df_order_info_order_id` FOREIGN KEY (`order_id`) REFERENCES `df_order_info` (`order_id`),
  CONSTRAINT `df_order_goods_sku_id_2135dc4f7368c4d3_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_order_goods`
--

LOCK TABLES `df_order_goods` WRITE;
/*!40000 ALTER TABLE `df_order_goods` DISABLE KEYS */;
INSERT INTO `df_order_goods` VALUES (1,'2021-01-28 03:23:03.091090','2021-01-28 03:23:03.091120',0,1,20.00,'','202101281123031',2),(2,'2021-01-28 11:48:21.226719','2021-01-28 11:48:21.226751',0,1,10.00,'','202101281948211',1),(3,'2021-01-28 12:30:34.640098','2021-01-28 12:30:34.640129',0,1,10.00,'安徽长丰县的草莓真不错！很新鲜','202101282030341',1);
/*!40000 ALTER TABLE `df_order_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_order_info`
--

DROP TABLE IF EXISTS `df_order_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_order_info` (
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `order_id` varchar(128) NOT NULL,
  `pay_method` smallint(6) NOT NULL,
  `total_count` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `transit_price` decimal(10,2) NOT NULL,
  `order_status` smallint(6) NOT NULL,
  `trade_no` varchar(128) NOT NULL,
  `addr_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `df_order_info_90ccbf41` (`addr_id`),
  KEY `df_order_info_e8701ad4` (`user_id`),
  CONSTRAINT `df_order_info_addr_id_19084a4768a1dfeb_fk_df_address_id` FOREIGN KEY (`addr_id`) REFERENCES `df_address` (`id`),
  CONSTRAINT `df_order_info_user_id_5c41bea286e53655_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_order_info`
--

LOCK TABLES `df_order_info` WRITE;
/*!40000 ALTER TABLE `df_order_info` DISABLE KEYS */;
INSERT INTO `df_order_info` VALUES ('2021-01-28 03:23:03.083898','2021-01-28 03:23:03.083927',0,'202101281123031',3,1,20.00,10.00,1,'',1,1),('2021-01-28 11:48:21.210128','2021-01-28 11:48:21.210176',0,'202101281948211',3,1,10.00,10.00,1,'',1,1),('2021-01-28 12:30:34.630437','2021-01-28 12:30:34.630489',0,'202101282030341',3,1,10.00,10.00,5,'2021012822001460180501082749',1,1);
/*!40000 ALTER TABLE `df_order_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_user`
--

DROP TABLE IF EXISTS `df_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `updata_time` datetime(6) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_user`
--

LOCK TABLES `df_user` WRITE;
/*!40000 ALTER TABLE `df_user` DISABLE KEYS */;
INSERT INTO `df_user` VALUES (1,'pbkdf2_sha256$20000$Y2BZAVJaJNUw$cUA4ySixeNwAg9bE7GT0HlMdlhw39SX4mpaQsMyXwN4=','2021-01-28 05:20:45.719086',1,'leon','','','ahcfl@qq.com',1,1,'2021-01-27 11:50:46.677147','2021-01-27 11:50:46.688881','2021-01-27 11:50:46.688896',0),(2,'pbkdf2_sha256$20000$xqnMY7iNBGIe$k/1z7+qYG5jXWrTVRVOKd+lqSJgI+as0C0KqyZ+q/U0=',NULL,0,'leon1','','','ahcfl@qq.com',0,0,'2021-01-27 13:08:20.020372','2021-01-27 13:08:20.032139','2021-01-27 13:08:20.032230',0),(3,'pbkdf2_sha256$20000$6UnZfUJB1W2L$0QhBpVpqg3xQFwUDwm/F5V1q9o2qBO7tYNta34Gadf8=',NULL,0,'leon2','','','ahcfl@qq.com',0,0,'2021-01-28 14:22:53.412082','2021-01-28 14:22:53.423863','2021-01-28 14:22:53.423877',0),(4,'pbkdf2_sha256$20000$xW2nFzDrshIE$fyebR2WyOG8aAoryXc3LQ6JHnuZliU53/+iGOF35yjk=',NULL,0,'leon3','','','ahcfl@qq.com',0,0,'2021-01-28 14:29:44.708032','2021-01-28 14:29:44.719908','2021-01-28 14:29:44.719924',0),(5,'pbkdf2_sha256$20000$01QF47l8EZ6F$SbQ9Cy8eI1O/76pe2PXCMj7VaE+kNWzCWjxS1jaXnqU=',NULL,0,'leon4','','','ahcfl@qq.com',0,0,'2021-01-29 02:55:55.861795','2021-01-29 02:55:55.874265','2021-01-29 02:55:55.874279',0),(6,'pbkdf2_sha256$20000$iLGugd15DCEX$qzz5+P/9S7ml1kwihuKNveQkIgm4B/Tbh5i4I+irN9M=','2021-01-29 03:14:15.666199',0,'leon5','','','ahcfl@qq.com',0,1,'2021-01-29 03:13:16.166334','2021-01-29 03:13:16.179053','2021-01-29 03:13:16.179066',0);
/*!40000 ALTER TABLE `df_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_user_groups`
--

DROP TABLE IF EXISTS `df_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `df_user_groups_group_id_6e423c74fd457dd5_fk_auth_group_id` (`group_id`),
  CONSTRAINT `df_user_groups_group_id_6e423c74fd457dd5_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `df_user_groups_user_id_3452dfed1b4446b5_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_user_groups`
--

LOCK TABLES `df_user_groups` WRITE;
/*!40000 ALTER TABLE `df_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `df_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `df_user_user_permissions`
--

DROP TABLE IF EXISTS `df_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `df_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `df_user_use_permission_id_227b5d217b1fc8f2_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `df_user_use_permission_id_227b5d217b1fc8f2_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `df_user_user_permissions_user_id_6d59682e39b098f3_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `df_user_user_permissions`
--

LOCK TABLES `df_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `df_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `df_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `djang_content_type_id_4a1a74acdad59fcc_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_17f7c80a86f465df_fk_df_user_id` (`user_id`),
  CONSTRAINT `djang_content_type_id_4a1a74acdad59fcc_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_17f7c80a86f465df_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2021-01-28 05:40:33.992627','1','吃货暑假趴',2,'已修改 image 。',14,1),(2,'2021-01-28 05:40:52.945412','2','盛夏尝鲜季',2,'已修改 image 。',14,1),(3,'2021-01-28 05:42:08.663788','1','新鲜水果',2,'已修改 image 。',8,1),(4,'2021-01-28 05:42:21.158569','2','海鲜水产',2,'已修改 image 。',8,1),(5,'2021-01-28 05:42:41.597844','3','猪牛羊肉',2,'已修改 image 。',8,1),(6,'2021-01-28 05:42:57.703545','4','禽类蛋品',2,'已修改 image 。',8,1),(7,'2021-01-28 05:43:07.132026','5','新鲜蔬菜',2,'已修改 image 。',8,1),(8,'2021-01-28 05:43:16.562739','6','速冻食品',2,'已修改 image 。',8,1),(9,'2021-01-28 06:13:35.292183','4','IndexGoodsBanner object',2,'已修改 image 。',12,1),(10,'2021-01-28 06:13:44.481992','3','IndexGoodsBanner object',2,'已修改 image 。',12,1),(11,'2021-01-28 06:13:55.556453','2','IndexGoodsBanner object',2,'已修改 image 。',12,1),(12,'2021-01-28 06:13:58.381218','2','IndexGoodsBanner object',2,'没有字段被修改。',12,1),(13,'2021-01-28 06:14:10.228255','1','IndexGoodsBanner object',2,'已修改 image 。',12,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_726507b065eb0103_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'contenttypes','contenttype'),(10,'goods','goods'),(11,'goods','goodsimage'),(9,'goods','goodssku'),(8,'goods','goodstype'),(12,'goods','indexgoodsbanner'),(14,'goods','indexpromotionbanner'),(13,'goods','indextypegoodsbanner'),(16,'order','ordergoods'),(15,'order','orderinfo'),(5,'sessions','session'),(7,'user','address'),(6,'user','user');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-01-27 08:49:48.038741'),(2,'contenttypes','0002_remove_content_type_name','2021-01-27 09:17:26.258876'),(3,'auth','0001_initial','2021-01-27 09:17:26.415755'),(4,'auth','0002_alter_permission_name_max_length','2021-01-27 09:17:26.434288'),(5,'auth','0003_alter_user_email_max_length','2021-01-27 09:17:26.440916'),(6,'auth','0004_alter_user_username_opts','2021-01-27 09:17:26.448326'),(7,'auth','0005_alter_user_last_login_null','2021-01-27 09:17:26.459314'),(8,'auth','0006_require_contenttypes_0002','2021-01-27 09:17:26.462275'),(9,'user','0001_initial','2021-01-27 09:17:26.567187'),(10,'admin','0001_initial','2021-01-27 09:17:26.608800'),(11,'goods','0001_initial','2021-01-27 09:17:26.942687'),(12,'order','0001_initial','2021-01-27 09:17:26.968446'),(13,'order','0002_auto_20201201_2216','2021-01-27 09:17:27.128881'),(14,'sessions','0001_initial','2021-01-27 09:17:27.144922');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-29 12:39:40
